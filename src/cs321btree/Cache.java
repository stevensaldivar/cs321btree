//package cs321btree;

import java.util.*;

public class Cache {
	private int size;
	private List<Object> list;
	private int numHits;
	private int numMiss;
	
	public Cache(int size)
	{
		list = Collections.synchronizedList(new LinkedList<>());
		this.size = size;
		numHits = 0;
		numMiss = 0;
	}
	
	/**
	 * Gets an object that is equal to data, uses .equals() to compare
	 * @param data 	- Object to compare
	 * @return		- Object in list matching data
	 */
	public Object getObject(Object data)
	{
		numHits++;
		
		if(list.contains(data))
		{
			int index = list.indexOf(data);
			return list.get(index);
		}
		else
		{
			numMiss++;
			return null;
		}
	}
	
	/**
	 * Adds object to cache
	 * @param data	- Object to add
	 * @return		- Object removed from cache if cache is full
	 */
	public Object addObject(Object data)
	{
		Object removed = null;
		if(size == list.size())
		{
			removed = list.remove(size - 1);
		}
		
		list.add(0, data);
		
		return removed;
	}
	
	/**
	 * Remove an object from cache
	 * @param data	- Object to remove from cache
	 * @return		- If removal was successful
	 */
	public Boolean removeObject(Object data)
	{
		return list.remove(data);
	}
	
	/**
	 * Clear the cache of all data
	 */
	public void clearCache()
	{
		list.clear();
		numHits = 0;
		numMiss = 0;
	}
	
	/**
	 * Get the numbers of successful object searches
	 * @return
	 */
	public int getNumHits()
	{
		return numHits;
	}
	
	/**
	 * Get the number of unsuccessful object searches
	 * @return
	 */
	public int getNumMiss()
	{
		return numMiss;
	}
	
	/**
	 * Get the ratio of hits to number of misses
	 * @return
	 */
	public double getHitsRatio()
	{
		return ((double)(numHits - numMiss)/numHits);
	}
	
	/**
	 * Gets 
	 * @return
	 */
	public boolean isEmpty()
	{
		if(list.size() == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public Object removeNext() 
	{
		Object obj = list.get(list.size() - 1);
		
		list.remove(list.size() - 1);
		
		return obj;
		
	}
}
