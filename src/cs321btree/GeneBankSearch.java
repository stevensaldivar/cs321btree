//package cs321btree;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
//import cs321btree.Cache;
//import cs321btree.Toolbox;

public class GeneBankSearch {

	public static void main(String[] args) {
		
		if(args.length < 3 || args.length > 5)							// Check for correct number of arguments
		{
			printSyntax("Number of treeFile arguments is incorrect");
			return;
		}
		
		String error = null;
		int cacheON, cacheSize = 0, debug = 0;
		
		try
		{
			error = "First argument must be an integer";
			cacheON = Integer.parseInt(args[0]);						// Parse cache input
			
			if(cacheON != 0 && cacheON != 1)							// Check cache input is valid
			{
				printSyntax("Argument must be a 1(No Cache) or a 0(With Cache)");
				return;
			}
			
			int temp = 4;
			if (cacheON == 1) {											// if using cache check for size
				if (args.length >= 4) {
					error = "Cache size must be an integer";
					cacheSize = Integer.parseInt(args[3]);

					if (cacheSize <= 0) {								// size must be positive
						printSyntax("Cache must have a size of 1 or greater");
						return;
					}
				}
				else
				{
					printSyntax("If cache is used, cache size must be defined");
				}
				temp = 5;
			}
			
			if(args.length == temp)										// Check debug level
			{
				error = "Debug level must be an integer";
				debug = Integer.parseInt(args[4]);
			}
		} catch (Exception c)
		{
			printSyntax(error);
			return;
		}
				
		try
		{ 
			String btreeFile = args[1];
			FileInputStream treeFile = new FileInputStream(btreeFile);	// open btree file
			
			byte[] bint = new byte[Integer.BYTES];						// byte array for ints
			treeFile.read(bint);
			int offset = ByteBuffer.wrap(bint).getInt();				// get metadata offset
			
			treeFile.read(bint);
			int degree = ByteBuffer.wrap(bint).getInt();				// get degree
			treeFile.read(bint);
			int length = ByteBuffer.wrap(bint).getInt();				// get sequence length
			treeFile.read(bint);
			int numNodes = ByteBuffer.wrap(bint).getInt();				// get number of nodes

			treeFile.close();
			
			Toolbox box = new Toolbox();
			Cache cache = null;
			
			if(cacheON == 1)
			{
				cache = new Cache(cacheSize);							// Create new cache of defined size
			}
			
			Node root = box.getNode(btreeFile, cache, offset, degree);
			
			treeFile.close();											// close tree
			
			
			BufferedReader searchFile = new BufferedReader(new FileReader(args[2]));	// open search sequences file
			String searchLine = searchFile.readLine();					// get first search
			
			while(searchLine != null)									// For each search query
			{	
				long searchValue = box.keyValue(searchLine);			// convert subsequence into long value
				
				boolean found = false;
				Node node = root;										// Keep root node in memory
				
				while(!found)
				{
					TreeObject[] keys = node.getKeys();
					TreeObject obj = null;
					
					for(int i =0; i < keys.length; i++)
					{
						if(keys[i].getFrequency() == 0)
						{
							obj = keys[i];
							i = keys.length;
						}
						else if(searchValue == keys[i].getSequence())
						{
							obj = keys[i];
							i = keys.length;
						}
					}
					
					if(obj == null || obj.getFrequency() == 0) 									// If not find the next node
					{
						int child = node.findChild(new TreeObject(searchValue));
						
						if (child != 0) {

							node = box.getNode(btreeFile, cache, child, degree);

							if (node == null) {
								System.out.println(searchLine + ": " + 0); // If not found print frequency as 0
								found = true;
								if (searchFile.ready()) {
									searchLine = searchFile.readLine().trim();
								} else {
									searchLine = null;
								}
							}
						}
						else
						{
							System.out.println(searchLine + ": " + 0); // If not found print frequency as 0
							found = true;
							if (searchFile.ready()) {
								searchLine = searchFile.readLine().trim();
							} else {
								searchLine = null;
							}
						}
					}
					else
					{
						System.out.println(searchLine + ": " + obj.getFrequency());			// If found print sequence with frequency
						found = true;
						if(searchFile.ready()) {
							searchLine = searchFile.readLine().trim();
						}
						else
						{
							searchLine = null;
						}
					}
				}
			}
			
			searchFile.close();
		}
		catch(FileNotFoundException e)
		{
			printSyntax("BTree File Not Found");
		}
		catch(IOException e)
		{
			System.out.println("Error Reading BTree File");
		}
		
		System.out.println("Finished");
		
	}
	
	/**
	 * Prints the syntax to use for this program, along with a custom error message for debugging
	 * 
	 * @param error 
	 * Custom error message to print with syntax
	 */
	private static void printSyntax(String error)
	{
		System.out.println(error);
		System.out.println("Use the Following Syntax:");
		System.out.println("java GeneBankSearch <0/1(no/with cache)> <btree file> <query file> [<cache size>] [<debug level>]");
	}

}
