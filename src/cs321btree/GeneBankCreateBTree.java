//package cs321btree;
import java.io.*;
//import cs321btree.BTree;

public class GeneBankCreateBTree {

	public static void main(String[] args) {

		// Checks if there are the correct number of input arguments
		if(args.length < 4 || args.length > 6)
		{
			printSyntax("Invalid number of arguments");
			return;
		}

		String error = null;						// Error string to print
		int cacheON, degree, length, size = 0, debug;		// Variables from arguments
		
		try
		{
			error = "First argument must be an integer";
			cacheON = Integer.parseInt(args[0]);		// Get cache variable
			
			// Check if input is acceptable
			if(cacheON != 0 && cacheON != 1)
			{
				printSyntax("Argument must be a 1(No Cache) or a 0(With Cache)");
				return;
			}
			
			error = "Degree must be an int";
			degree = Integer.parseInt(args[1]);		// Get degree
			
			// Check if greater than 0
			if(degree < 0)
			{
				printSyntax("Degree must be 0 or a positive integer");
				return;
			}
			
			error = "Sequence length must be an Integer";
			length = Integer.parseInt(args[3]);		// Get sequence length
			
			// Check if length is acceptable
			if(length <= 0 || length > 31)
			{
				printSyntax("Sequence length must be between 1 and 31");
				return;
			}
			
			int temp;
			if(cacheON == 1)							// If using cache 
			{	
				if (args.length > 4) 				// Define Size
				{
					error = "Cache size must be an integer";
					size = Integer.parseInt(args[4]);

					if (size <= 0) {
						printSyntax("Cache must have a size of 1 or greater");
						return;
					}
				}
				else {
					printSyntax("If cache is implemented, size must be defined");

				}
				
				temp = 5;
			}
			else
			{
				temp = 4;
			}
			
			if (args.length > temp) {				// Check Debug Level
				error = "Debug level must be an integer";
				debug = Integer.parseInt(args[5]);

				if (debug != 0 && debug != 1) {
					printSyntax("Debug level must be a 1(Dump File) or a 0(Error Stream)");
					return;
				}
			} else {
				debug = 0;
			}
		} catch (Exception c)						// Print syntax if error occurs
		{
			printSyntax(error);
			return;
		}
				
		String btreeFile = args[2] + ".btree.data." + length + "." + degree;	// Btree file name
		int offset = 16;													// Offset according to btree file layout
		
		try 
		{
			Toolbox box = new Toolbox();
			box.writeMetaData(btreeFile, offset, degree, length, 1);
		}
		catch(IOException e)
		{
			System.out.println("Error writing BTree Binary File");
		}
		
		Cache cache = null;
		if(cacheON == 1)
		{
			cache = new Cache(size); 
		}
		
		try
		{
			createBTreeFromFile(args[2], degree, length, cache, btreeFile);		// Create btree from file
		} 
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
			printSyntax("\nFile Not Found");
			return;
		} 
		catch(IOException e)
		{
			System.out.println("File Read Error");
			System.out.println(e.toString());
			e.printStackTrace();
			return;
		}
		
		System.out.println("\nFinished");
		
	}

	/**
	 * Prints syntax to use the program and a custom error message for debugging
	 * @param error - custom message to print
	 */
	private static void printSyntax(String error)
	{
		System.out.println(error);									// Print input error
		System.out.println("Use the Following Syntax:");			// Print syntax
		System.out.println("java GeneBankCreateBTree <0/1(no/with cache)> <degree> <gbk file> <sequence length> [<cache size>] [<debug level>]");
	}
	
	/**
	 * Parses a string a leaves only DNA sequences
	 * @param line	- String to parse
	 * @return		- DNA Partial Sequence
	 */
	private static String trimLine(String line)
	{	
		line = line.toLowerCase();					//  Lower case
		char c;
		String output = "";
		for(int i = 0; i < line.length(); i++)		// Look at each char
		{
			c = line.charAt(i);						
			if(c == 'a' || c == 'c' || c == 'g' || c == 't' || c == '/' || c == 'n')	// Check if valid char
			{
				output = output + line.charAt(i);
			}
		}
		
		return output;
	}

	
	/**
	 * Creates a BTree Binary File from an input file
	 * @param InputFileName	- Name of the DNA input file to use to create a BTree file
	 * @param degree		- Degree of the BTree
	 * @param length		- Length of the DNA sub Sequences
	 * @return				- BTree object
	 * @throws IOException
	 */
	private static BTree createBTreeFromFile(String InputFileName, int degree, int length, Cache cache, String btreeFile) throws IOException 
	{
		BTree tree = new BTree(degree, cache, length, btreeFile);
		BufferedReader reader = new BufferedReader(new FileReader(InputFileName));	// open file CHANGED

		String line = reader.readLine();									// Get first line
		
		while (reader.ready()) {											// While there are more lines
			while (!line.equals("ORIGIN") && reader.ready()) {				// Find DNA sequence beginning
				line = reader.readLine().trim();							// Trim white spaces
			}
			
			if (reader.ready()) {

				line = trimLine(reader.readLine().trim());					// Get DNA sequence
				String[] sequences = setupSequencyArray(length, line);		// Fill array

				int ltag = length;											// Position in line string 
				int stag = 0;												// position in sequence array

				while (!line.equals("//")) {								// go to end of sequence
					tree.insert(sequences[stag]);							// insert finished sub sequence into btree
					sequences[stag] = "";									// clear position for next subsequence
					stag++;					

					if (stag == sequences.length) {							// wrap around sequence array
						stag = 0;
					}

					for (int i = 0; i < sequences.length; i++) {			// Add next char to all of the subsequences currently building
						sequences[i] = sequences[i] + line.charAt(ltag);
					}

					ltag++;											
					if (ltag == line.length()) {							// Get next line 
						line = trimLine(reader.readLine().trim());
						ltag = 0;
						System.out.print(".");
					}

				}
				
				tree.insert(sequences[stag]);								// Insert last complete subsequence into btree
				
			}
		}

		reader.close();			// close input file reader

		Toolbox box = new Toolbox();
		if(cache != null) {
			while (!cache.isEmpty()) {
				Node node = (Node) cache.removeNext();
				box.writeNode(btreeFile, node, tree.root().getOffset(), degree, length, tree.numberOfNodes());
			}
		}
		
		return tree;
	}

	/**
	 * Sets up an array to rapidly create subsequences to insert into a BTree 
	 * @param length	- Length of DNA subsequences
	 * @param line		- String to start creating the array from
	 * @return			- Array of strings containing started subsequences that are ready to build
	 */
	private static String[] setupSequencyArray(int length, String line)
	{
		String[] sequences = new String[length];				// New string array 
		
		for(int i = 0; i < length; i++)							// Initialize array
		{
			sequences[i] = "";								
		}
		
		for(int i = 0; i < length; i++)							
		{
			for(int j = 0; j <= i; j++)
			{
				sequences[j] = sequences[j] + line.charAt(i);	// setup by adding first char to space 0, second to 0 and 1....
			}
		}
		
		return sequences;
	}
	
}
