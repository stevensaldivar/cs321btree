//package cs321btree;

public class TreeObject {
	private long sequence;
	private int frequency;
	
	public TreeObject(long sequence) {
		this.sequence = sequence;
		frequency = 1;
	}
	
	/**
	 * gets the DNA sequence, previously converted to binary
	 * @return long DNA binary sequence
	 */
	public long getSequence() {
		return this.sequence;
	}
	
	/**
	 * Set the DNA sequence... If you want? Dunno if useful yet
	 * @param sequence to be set to
	 */
	public void setSequence(long sequence) {
		this.sequence = sequence;
	}
	
	/**
	 * Checks to see if sequence in this Node is equal to sequence in other node
	 * @param obj Node with sequence to be compared to
	 * @return boolean, true if equal, false if not
	 */
	public boolean equals(TreeObject obj) {
		return (this.getSequence() == obj.getSequence());
	}

	/**
	 * compares this.sequence to another sequence.
	 * @param obj, other TreeObjects sequence to be compared to.
	 * @return -1 if this.sequence is LESS than param, +1 if MORE than param, 0 if equal.
	 *         -2 if something weird af happens and I don't know.
	 */
	public int compareTo(TreeObject obj){
		//Converting long sequences to Strings, to ints for comparison.
		/*String sLocal = Long.toString(this.getSequence());
		String sOther = Long.toString(obj.getSequence());

		int iLocal = Integer.parseInt(sLocal, 2);
		int iOther = Integer.parseInt(sOther, 2);
		*/
		
		long local = this.getSequence();
		long other = obj.getSequence();

		if(local < other){
			return -1;
		}
		if(local > other){
			return 1;
		}
		else if(local == other){
			return 0;
		}
		return -2;
	}
	
	/**
	 * returns string representation of sequence
	 */
	public String toString() {
		return Long.toString(this.sequence);
	}
	
	/** 
	 * returns frequency, amount of times this sequence has occurred thus far
	 * @return frequency
	 */
	public int getFrequency() {
		return this.frequency;
	}
	
	/**
	 * increments int frequency
	 */
	public void incFrequency() {
		this.frequency++;
	}
	
	/**
	 * increments int frequency
	 */
	public void setFrequency(int i) {
		this.frequency = i;
	}
	
	
}
