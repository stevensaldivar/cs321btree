//package cs321btree;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public class Toolbox {
	
	
	public void ToolBox()
	{

	}
	
	/**
	 * Converts a DNA sequence into a long value for easy storage
	 * 
	 * @param value
	 * DNA Sequence to convert to a long value
	 * 
	 * @return
	 * Long value representation of the DNA Sequence
	 */
	public long keyValue(String value)
	{	
		value = value.toLowerCase();
		long key = 0;
		String temp = "";			//old implementation, will not work, but I kept it here just in case
		
		for(int i = 0; i < value.length(); i++)
		{
			char c = value.charAt(i);			
			switch (c)
			{
			case 'a':
				temp = temp + "00";
				break;
			case 'c':
				temp = temp + "01";
				break;
			case 'g':
				temp = temp + "10";
				break;
			case 't':
				temp = temp + "11";
				break;
			}
		}
		
		try {
			key = Long.parseLong(temp, 2);
		} catch(Exception e)
		{
			key = 0;
		}
		
		return key;
	}
	
	/**
	 * Creates a BTree node from a BTree binary file
	 * 
	 * @param fileName		- Name of BTree binary file to open
	 * @param degree		- Degree of the BTree
	 * @param nodeOffset	- Offset of the node to be read from the BTree file
	 * @return				- A BTree node from the specified offset
	 * @throws IOException
	 */
	public Node createNode(String fileName, int degree, int nodeOffset) throws IOException {
		TreeObject[] objects = new TreeObject[2 * degree - 1];
		byte[] blong = new byte[Long.BYTES];
		byte[] bint = new byte[Integer.BYTES];
		FileInputStream BTreeFile = new FileInputStream(fileName);
		//int ava = BTreeFile.available();
		
		//byte[] meta = new byte[nodeOffset];
/*		
		BTreeFile.read(meta);

		meta = new byte[32*degree-8];
		
		BTreeFile.read(meta);
		BTreeFile.read(meta);
		BTreeFile.read(meta);
		BTreeFile.read(meta);
		BTreeFile.read(meta);
		BTreeFile.read(meta);
		BTreeFile.read(meta);
		BTreeFile.read(meta);
*/
		
		BTreeFile.skip(nodeOffset);
		
		for (int i = 0; i < objects.length; i++) {
			BTreeFile.read(blong);
			objects[i] = new TreeObject(ByteBuffer.wrap(blong).getLong());
			BTreeFile.read(bint);
			objects[i].setFrequency(ByteBuffer.wrap(bint).getInt());
		}

		int[] children = new int[2 * degree];

		for (int i = 0; i < children.length; i++) {
			BTreeFile.read(bint);
			children[i] = ByteBuffer.wrap(bint).getInt();
		}

		// Node node = new Node(degree, objects, nodes);
		Node node = new Node(degree, objects, children, nodeOffset);
		
		BTreeFile.read(bint);
		
		node.setParent(ByteBuffer.wrap(bint).getInt());
		
		BTreeFile.close();
		
		return node;

	}
	
	/**
	 * Writes a Node to a BTree Binary File
	 * @param writer		- open file writer
	 * @param node			- node to write
	 * @param offset		- offset of the node
	 * @throws IOException
	 */
	public void writeNode(String btreeFile, Node node, int rootOffset, int degree, int length, int nodeNum) throws IOException
	{
		TreeObject[] keyValues = node.getKeys();
		ByteBuffer bint = ByteBuffer.allocate(Integer.BYTES);		// byte buffer for int
		ByteBuffer blong = ByteBuffer.allocate(Long.BYTES);			// byte buffer for long
		int nodeSize = 32*degree-8;
		byte[] bNode = new byte[nodeSize];
		String newFile = "newFile";
		int[] children = node.returnChildren();
		
		this.writeMetaData(newFile, rootOffset, degree, length, nodeNum);
		
		FileOutputStream writer = new FileOutputStream(newFile, true);
		FileInputStream reader = new FileInputStream(btreeFile);
		reader.skip(rootOffset);
		int nodeCount = 0;
		
		while (nodeCount <= nodeNum) {
					
			if (rootOffset + nodeCount*nodeSize != node.getOffset()) {

				reader.read(bNode);
				writer.write(bNode);
				
			} else if (keyValues[0] != null){

				reader.skip(nodeSize);
				
				byte[] arra = blong.putLong(keyValues[0].getSequence()).array();
				writer.write(arra);
				arra = bint.putInt(keyValues[0].getFrequency()).array();
				writer.write(arra);
				blong.clear();
				bint.clear();

				for (int i = 1; i < keyValues.length; i++) // Write keyvalues and frequencies
				{
					if (keyValues[i] != null) {
						writer.write(blong.putLong(keyValues[i].getSequence()).array());
						writer.write(bint.putInt(keyValues[i].getFrequency()).array());
						blong.clear();
						bint.clear();
					} else {
						writer.write(blong.putLong(0).array());
						writer.write(bint.putInt(0).array());
						blong.clear();
						bint.clear();
					}
				}
				
				for (int i = 0; i < children.length; i++) // Write children offset
				{
					if(children[i] != 0) {
					writer.write(bint.putInt(children[i]).array());
					bint.clear();
					}
					else
					{
						writer.write(bint.putInt(0).array());
						bint.clear();
					}
				}
				
				writer.write(bint.putInt(node.getParentOffset()).array());
				
			}
			
			nodeCount++;

		}
		
		writer.close();
		reader.close();
		
		File file = new File(btreeFile);
		file.delete();
		
		file = new File(newFile);
		file.renameTo(new File(btreeFile));
		
	}
	
	public void writeMetaData(String btreeFile, int offset, int degree, int length, int numNodes) throws IOException
	{
		FileOutputStream writer = new FileOutputStream(btreeFile, false);		// open file
		ByteBuffer b = ByteBuffer.allocate(Integer.BYTES);				// Byte buffer for ints
		writer.write(b.putInt(offset).array());							// Write offset
		b.clear();
		writer.write(b.putInt(degree).array());							// Write degree
		b.clear();
		writer.write(b.putInt(length).array());							// Write sequence length
		b.clear();
		writer.write(b.putInt(numNodes).array());
		b.clear();
		
		writer.close();
	}
	
	public Node getNode(String btreeFile, Cache cache, int offset, int degree)
	{
		Node node = null;
		if(cache == null)
		{
			try {
				node = this.createNode(btreeFile, degree, offset);
			} catch (IOException e) {
				System.out.println(e.toString());				
				e.printStackTrace();
			}
		}
		else
		{
			node = (Node)cache.getObject(new Node(degree, offset));
			if(node == null)
			{
				try {
					node = this.createNode(btreeFile, degree, offset);
				} catch (IOException e) {
					System.out.println(e.toString());				
					e.printStackTrace();
				}
			}
		}
		
		return node;
	}
	
	public void saveNode(String btreeFile, Cache cache, Node node, int degree, int rootOffset, int length, int nodeNum)
	{
		if(cache == null)
		{
			try {
				this.writeNode(btreeFile, node, rootOffset, degree, length, nodeNum);
			} catch (IOException e) {
				System.out.println(e.toString());
				e.printStackTrace();
			}
		}
		else
		{
			Node temp = (Node)cache.getObject(node);
			
			if (temp == null) {

				temp = (Node) cache.addObject(node);
				if (temp != null) {
					try {
						this.writeNode(btreeFile, temp, rootOffset, degree, length, nodeNum);
					} catch (IOException e) {
						System.out.println(e.toString());
						e.printStackTrace();
					}
				}
			} else {
				cache.removeObject(temp);
				cache.addObject(temp);
			}
		}
	}
}
