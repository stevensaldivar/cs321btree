//package cs321btree;


public class Node {

	//4-22 Changed long[] keys to TreeObject[] To account for storage of these objects rather than longs
	private TreeObject[] keys;
	private int[] children;
	private int degree;
	private int parent;
	private int offset;			// 4/23 used to find node in the BTree file, and used to compare in .equals()
	
	
	public Node(int degree, int nodeOffset)
	{
		this.keys = new TreeObject[2*degree-1];
		this.children = new int[2*degree];
		this.degree = degree;
		this.offset = nodeOffset;
	}
	
	public Node(int degree, TreeObject[] keys, int[] children, int nodeOffset) {
		this.degree = degree;
		this.keys = keys;
		this.children = children;
		this.offset = nodeOffset;
    }
	
	public int insert(TreeObject obj)
	{
		int index = 0;
		
		if(!this.isFull()) {
			for(int i = 0; i < keys.length; i++) {
				if (keys[i] !=  null && keys[i].compareTo(obj) == 0)
				{
					keys[i].incFrequency();
					i = keys.length;
				}
				else if (keys[i] == null || keys[i].compareTo(obj) < 0) {
					this.shiftKeys(i);
					keys[i] = obj;
					i = keys.length;
				}
				else
				{
					index++;
				}
			}
		}
		
		return index;
	}

	private void shiftKeys(int index){
		
		if(index == keys.length - 1) {
			return;
		}
		
		TreeObject temp = keys[index];
		int tempChild = children[index];
		//keys[index] = null;
		for(int i = keys.length-1; i > index; i--){
			keys[i] = keys[i-1];
			children[i] = children[i-1];
		}
		keys[index+1] = temp;
		children[index+1] = tempChild;
		
	}

	/**
	 * sets this.parent to param parent
	 * @param parent, node to be set to parent
	 */
	public void setParent(int parent) {
		this.parent = parent;
	}

	/**
	 * Check to see if children[] has contents
	 * @return boolean, true if has children
	 */
	public boolean hasChildren()
	{
		if(children[0] == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	/**
	 * Finds which child it would be appropriate to insert a long value into
	 * @param obj, value that is to be inserted
	 * @return child node that value is to be inserted into
	 */
	public int findChild(TreeObject obj)
	{
		for(int i = 0; i < keys.length; i++)
		{
			if(keys[i] != null && keys[i].getFrequency() == 0)
			{
				return children[i];
			}
			
			if(keys[i] != null && keys[i].compareTo(obj) == 0)
			{
				keys[i].incFrequency();
				return this.offset;
			}
			else if(keys[i] == null || keys[i].compareTo(obj) < 0)
			{
				return children[i];
			}
		}		
		return children[children.length - 1];		
	}

	//TODO Not sure if correct
	/**
	 * adds child to children array?
	 * @param child to be added
	 */
	public void addChild(int child){
	    int curr = countChildren();
	    if(curr != this.children.length){
	        children[curr] = child;
        }
    }

	/**
	 * counts to see how many children in children[]
	 * @return count
	 */
	public int countChildren(){
	    int i = 0;
	    while(children[i] != 0){
	        i++;
        }
        return i;
    }
	
	
	
	/**
	 * How many keys are currently in a node
	 * @return number of keys in the node
	 */
	public int numberKeys()
	{
		int count = 0;
		for(int i = 0; i < keys.length; i++) {
			if(keys[i] != null) {
				count++;
			}
		}
		return count;
	}

	/**
	 * References degree vs number of entries in keys[]
	 * @return true if full
	 */
	public boolean isFull() {
		return numberKeys() == (2*degree -1);
	}

	//TODO Figure out return
	/**
	 * Splits node. Pushes middle value up to parent, or creates new node
	 * if parent if node is the top
	 * @return pointer to something, undecided
	 */
	public Node split(int numNodes, int rootOffset, int nodeSize, Cache cache, String btreeFile, int length)
	{
		Node rightNode = new Node(this.degree, (numNodes + 1)*nodeSize + rootOffset);
		Toolbox box = new Toolbox();
		
		for(int i = degree; i < keys.length;i++) {
			rightNode.insert(keys[i]);
			keys[i] = null;
		}
		//obj that is gonna go up
		TreeObject up = keys[degree-1];
		
		if(this.parent != 0) {
			Node parentNode = box.getNode(btreeFile, cache, rootOffset, length);
			int i = parentNode.insert(up);
			rightNode.setParent(parent);
			parentNode.insertChild(this.offset, i);
			parentNode.insertChild(rightNode.offset, i+1);
			box.saveNode(btreeFile, cache, rightNode, degree, rootOffset, length, numNodes);
		} else {
			Node leftNode = new Node(this.degree, (numNodes + 2)*nodeSize + rootOffset);
			for(int i = 0; i < degree-1;i++) {
				leftNode.insert(keys[i]);
			}
			keys = new TreeObject[2*degree - 1];
			this.insert(up);
			
			rightNode.setParent(this.getOffset());
			leftNode.setParent(this.getOffset());
			
			this.insertChild(leftNode.getOffset(), 0);
			this.insertChild(rightNode.getOffset(), 1);
			
			box.saveNode(btreeFile, cache, rightNode, degree, rootOffset, length, numNodes);
			box.saveNode(btreeFile, cache, leftNode, degree, rootOffset, length, numNodes);
		}
		keys[degree - 1] = null;

		box.saveNode(btreeFile, cache, this, degree, rootOffset, length, numNodes);

		Node parentNode = box.getNode(btreeFile, cache, rootOffset, degree);

		return parentNode;
	}
	
	public int[] returnChildren()
	{
		return children;
	}

	/**
	 * Put child nodes in children array
	 * @param child
	 */
	public void insertChild(int child, int indexKeyValue) {
		children[indexKeyValue] = child;
	}
	
	//Return true if node contains key value
	public boolean findKey(long value)
	{
		for(int i = 0; i < keys.length; i++) {
			if(keys[i].getSequence() == value) {
				return true;
			}
		}
		return false;
	}

	
	public TreeObject[] getKeys()
	{
		return keys;
	}
	
	public int getOffset()
	{
		return offset;
	}
	
	public int getParentOffset()
	{
		return parent;
	}
	
	public boolean equals(Object obj)
	{
		if(offset == ((Node)obj).getOffset())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
