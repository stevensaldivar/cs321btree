//package cs321btree;
import java.io.IOException;

//import cs321btree.Node;

public class BTree {

	private int degree;
	private int length;
	private Node root;
	private int numberNodes;
	private Toolbox tools;
	private Cache cache;
	private String btreeFile;
	
	public BTree(int degree, int length)
	{
		if (degree == 0)
		{
			this.degree = calDegree();
		}
		else
		{
			this.degree = degree;
		}
		numberNodes = 1;
		root = new Node(degree, 16);
		tools = new Toolbox();
		this.length = length;
	}
	
	public BTree(int degree, Cache cache, int length, String btreeFile)
	{
		this.degree = degree;
		this.cache = cache;
		this.btreeFile = btreeFile;
		if (degree == 0)
		{
			this.degree = calDegree();
		}
		else
		{
			this.degree = degree;
		}
		root = new Node(degree, 16);
		tools = new Toolbox();
		numberNodes = 1;
		this.length = length;
	}
	
	public void insert(String x) throws IOException
	{
		long value = tools.keyValue(x);
		TreeObject newSequence = new TreeObject(value);
		
		Node node = root;
		
		while(node.hasChildren())
		{
			if(node.numberKeys() == degree)
			{
				node.split(numberNodes, root.getOffset(), 32*degree-8, cache, btreeFile, length);
				if(numberNodes == 1)
				{
					numberNodes++;
					root = node;
				}
				numberNodes++;
			}
			
			int child = node.findChild(new TreeObject(value));
			
			if(child == node.getOffset())
			{
				return;
			}
			
			node = tools.getNode(btreeFile, cache, child, degree);
			
		}
		
		if(node.numberKeys() == (2*degree - 1))
		{
			node = node.split(numberNodes, root.getOffset(), 32*degree-8, cache, btreeFile, length);
			if(numberNodes == 1)
			{
				numberNodes++;
			}
			numberNodes++;
			int child = node.findChild(new TreeObject(value));
			if(child == node.getOffset())
			{
				return;
			}
			node = tools.getNode(btreeFile, cache, child, degree);
		}
		
		node.insert(newSequence);
		
		tools.saveNode(btreeFile, cache, node, degree, root.getOffset(), length, numberNodes);

	}
	
	// We need a function that returns the number of nodes
	public int numberOfNodes()
	{
		return numberNodes;
	}
	
	// Returns the root node
	public Node root()
	{
		return root;
	}
	
	private int calDegree()
	{
		int degree = 129;
		return degree;
	}
}
